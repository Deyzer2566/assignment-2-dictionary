SHELL:=bash
main.o: main.asm words.inc colon.inc
	nasm -g -felf64 main.asm
lib.o: lib.asm
	nasm -g -felf64 lib.asm
dict.o: dict.asm
	nasm -g -felf64 dict.asm
myapp: main.o lib.o dict.o
	ld dict.o lib.o main.o -o myapp
test: myapp
	@test1="key" ; \
	value1="value1" ; \
	test2="notkey" ; \
	value2="There's no string with this key" ; \
	test3="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstu" ; \
	value3='Value of key with maximum size' ; \
	right_tests=0 ; \
	for i in 1 2 3 ; \
	do\
        	test="test"$$i ; \
	        value="value"$$i ; \
		if [ "$$(echo $${!test} | ./myapp 2>&1)" = "$${!value}" ] ; \
	        then \
	                echo "TEST $$i PASSED (list[\"$${!test}\"]==\"$${!value}\")"; \
        	        right_tests=$$((right_tests+1)); \
	        else \
	                echo "TEST $$i DIDNT PASS" ; \
        	fi \
	done ; \
	echo "TESTS PASSED: $$right_tests/3" ; \
