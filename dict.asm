%include "lib.inc"
global find_word
section .text
;rdi - указатель на искомую строку
;rsi - указатель на первый элемент
find_word:
	.p:
		mov rax, qword[rsi]
		test rax, rax
		jz .exit
		push rsi
		add rsi, 8 ; указатель на ключ
		push rdi
		call string_equals
		pop rdi
		pop rsi
		test rax, rax
		mov rax, rsi
		jne .exit
		mov rsi, qword[rsi]
		jmp .p
	.exit:
	ret
