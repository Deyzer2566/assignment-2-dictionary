%define lists_begin 0
%macro colon 2
	%2: dq lists_begin
	db %1, 0
	%define lists_begin %2
%endmacro
