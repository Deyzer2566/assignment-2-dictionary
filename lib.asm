global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rax, rdi
    .loop:
    	mov dl, byte[rax]
	test dl, dl
	jz .exit
        inc rax
	jmp .loop	
    .exit:
	sub rax, rdi
    	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdi, rsp
    call print_string
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rdi, 10
    mov rsi, rsp
    sub rsp, 32
    dec rsi
    mov byte[rsi], 0
    .loop:
        mov rdx, 0
        div rdi
        add dl, '0'
	dec rsi
	mov byte[rsi],dl
        test rax, rax
        jz .end
        jmp .loop
    .end:
    mov rdi, rsi
    call print_string
    add rsp, 32
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov rax, rdi
    test rax, rax
    jns .positive ; я пытался использовать jae, но почему-то неработает :(
    .negative:
        mov rdi, '-'
	push rax
	call print_char
	pop rax
	neg rax
    .positive:
        mov rdi, rax
	call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    .loop:
        mov dl, byte[rdi]
	mov cl, byte[rsi]
	cmp cl, dl
	jnz .notequal
	test dl, dl
        jz .equal
	inc rdi
	inc rsi
	jmp .loop
    .equal:
        mov rax, 1
	ret
    .notequal:
        mov rax, 0
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, 0
    sub rsp, 16
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    test rax, rax
    ja .end
    mov rax, 0
    add rsp, 16
    ret
    .end:
    pop rax
    add rsp, 8
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    ;rdi - адрес начала
    ;rsi - размер буфера
    mov rax, 0
    push rdi
    push rax ; является ли символ началом слова
    .reading:
        cmp rsi, 1
        jz .word_is_very_long
	push rsi
	push rdi
        call read_char
	pop rdi
	pop rsi
        test rax, rax
        jz .end
        cmp rax, 0x20
        jz .spacesym
        cmp rax, 0x9
        jz .spacesym
        cmp rax, 0xA
        jz .spacesym
    .put:
        mov byte[rdi], al
        inc rdi
	pop rax
	mov rax, 1
        push rax
        dec rsi
    .dontput:
        jmp .reading
    .spacesym:
        pop rax
        test rax, rax
	push rax
        jz .dontput
        jmp .end
    .word_is_very_long:
        mov rax, 0
	mov byte[rdi], 0
	add rsp, 16
	ret
    .end:
        pop rax
        mov byte[rdi], 0
        pop rax
	sub rdi, rax
	mov rdx, rdi
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push rdi
    mov rax, 0
    mov rsi, 0
    mov rcx, 10
    mov rdx, 0
    .find_uint:
        mov sil, byte[rdi]
        cmp sil, '0'
        jb .end
        cmp sil, '9'
        ja .end
        inc rdi
        mul rcx
        sub sil, '0'
        add rax, rsi
        jmp .find_uint
    .end:
        mov rdx, rdi
	pop rdi
	sub rdx, rdi
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    jz .negative
    .positive:
        call parse_uint
	ret
    .negative:
        inc rdi
	call parse_uint
	neg rax
        inc rdx
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdx
    mov rcx, 0
    .loop:
        test rdx, rdx
	jz .theresnosize
	dec rdx
        mov cl, byte[rdi]
	mov byte[rsi], cl
	test cl, cl
	jz .end
        inc rsi
	inc rdi
        jmp .loop
    .theresnosize:
	add rsp, 8
	mov rax, 0
	ret
    .end:
        pop rcx
	sub rcx, rdx
	mov rax, rcx
    ret
