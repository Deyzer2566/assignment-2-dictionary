%include "colon.inc"

section .rodata
colon "test3 key", test3
db "value3", 0

colon "test2 key", test2
db "value2", 0

colon "key", test1
db "value1", 0

colon "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstu", length_test
db "Value of key with maximum size", 0
