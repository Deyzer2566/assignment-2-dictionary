%include "words.inc"
%include "lib.inc"
%include "dict.inc"

section .rodata
error_message: db "There's no string with this key", 0xa

section .bss
buffer: resb 256

section .text
	global _start
_start:
	mov rax, 0
	mov rdi, 0
	mov rsi, buffer
	mov rdx, 256
	syscall

	test rax, rax
	jb .end
	
;	.loop:
	mov byte[buffer+rax-1],0 ; делаем -1, тк rax = длина (сообщение + \r)
	mov rdi, buffer
	mov rsi, length_test
	call find_word
	test rax, rax
	je .error
	add rax, 8
	push rax
	mov rdi, rax
	call string_length
	pop rdi
	add rdi, rax
	inc rdi
	call print_string
	call print_newline
	jmp .end
	.error:
		mov rax, 1
		mov rdi, 2
		mov rsi, error_message
		mov rdx, 32
		syscall
	.end:
	mov rax, 60
	mov rdi, 0
	syscall
